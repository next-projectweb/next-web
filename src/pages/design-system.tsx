import { Seo } from '@/ui/components/Seo'
import { Navigaion } from '@/ui/components/navigation/Navigation'
import { Avartar } from '@/ui/design-system/avatar/Avatar'
import { Buttons } from '@/ui/design-system/button/Buttons'
import { Logo } from '@/ui/design-system/logo/Logo'
import { Spinner } from '@/ui/design-system/spinner/Spinner'
import { Typography } from '@/ui/design-system/typographie/typography'
import { Container } from "@/ui/components/container/container";
import React from 'react'
import { FaUser, FaUsersRectangle } from 'react-icons/fa6'
import { MdVerifiedUser } from 'react-icons/md'

export default function DesignSystem() {
    return (
        <>
            <Navigaion />
            <Container className=" flex items-center  flex-col ">

                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded"> <Spinner />
                    <Spinner size="medium" />
                    <Spinner size="large" />
                </div>


                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded">

                    <Buttons>
                        accent
                    </Buttons>
                    <Buttons variant="secondary">
                        secondary
                    </Buttons>

                    <Buttons variant="outline">
                        accent
                    </Buttons>

                    <Buttons variant="disableds">
                        accent
                    </Buttons>

                </div>

                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded">  <Buttons isloading>
                    accent
                </Buttons>
                    <Buttons isloading variant="secondary" size="large">
                        secondary
                    </Buttons>

                    <Buttons isloading variant="outline" size="small">
                        accent
                    </Buttons>

                    <Buttons isloading variant="disableds" size="medium">
                        accent
                    </Buttons>

                    <Buttons isloading size="small" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>
                    <Buttons isloading size="medium" variant="ico" icon={{ icon: FaUsersRectangle }}>
                        icon
                    </Buttons>
                    <Buttons isloading size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>

                    <Buttons isloading icontheme="secondary" size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>

                    <Buttons isloading icontheme="gray" size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>

                    <Buttons isloading size="small" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                        icon
                    </Buttons>

                    <Buttons isloading size="small" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                        icon
                    </Buttons>

                    <Buttons isloading size="large" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                        icon
                    </Buttons>
                </div>







                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded"><Buttons>
                    accent
                </Buttons>
                    <Buttons variant="secondary" size="large">
                        secondary
                    </Buttons>

                    <Buttons variant="outline" size="small">
                        accent
                    </Buttons>

                    <Buttons variant="disableds" size="medium">
                        accent
                    </Buttons></div>



                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded"><Buttons size="small" variant="ico" icon={{ icon: FaUser }}>
                    icon
                </Buttons>
                    <Buttons size="medium" variant="ico" icon={{ icon: FaUsersRectangle }}>
                        icon
                    </Buttons>
                    <Buttons size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>

                    <Buttons icontheme="secondary" size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons>

                    <Buttons icontheme="gray" size="large" variant="ico" icon={{ icon: FaUser }}>
                        icon
                    </Buttons></div>

                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded">   <Buttons size="small" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                    icon
                </Buttons>

                    <Buttons size="small" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                        icon
                    </Buttons>

                    <Buttons size="large" icon={{ icon: MdVerifiedUser }} iconPosition="left">
                        icon
                    </Buttons></div>


                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded"> <Logo size="large" />
                    <Logo size="small" />
                    <Logo size="medium" />
                    <Logo size="very-small" /></div>


                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded">
                    <Avartar src="/assets/images/avatar2.png" size="small" alt="avatar" />
                    <Avartar src="/assets/images/avatar2.png" alt="avatar" />

                    <Avartar src="/assets/images/ulrich2.png" alt="avatar" size="large" />
                </div>

                <div className="flex  items-center gap-2 p-5 border border-gray-500 rounded">  <Typography variant="h4" className="text-primary-400"> bonjour</Typography>
                    <Typography variant="h1" className="text-primary-400"> bonjour</Typography>
                    <Typography variant="h2" className="text-primary-400"> bonjour</Typography>
                    <Typography className="text-primary-400" components="h3"> bonjour</Typography>

                    <Typography variant="caption1" className="text-primary-400"> bonjour</Typography>
                    <Typography variant="caption2" className="text-primary-400"> bonjour</Typography>
                    <Typography variant="caption3" className="text-primary-400"> bonjour</Typography>
                    <Typography variant="caption4" className="text-primary-400"> bonjour</Typography></div>


                {/*<div className="py-5  flex items-center justify-center flex-col"  >
  
     
         <Typography component="div" variant="h1"  >
        ulrich code HHHH
      </Typography>
      <Typography component="div" variant="h2"  >
        ulrich code HHHH4
      </Typography>

      <Typography component="h1" variant="body-lg" className="text-primary-400" >
        ulrich code
      </Typography>

      <Typography component="div" variant="h3"  >
        ulrich code
      </Typography>

      <Typography component="div" variant="caption2"  >
        ulrich code 1
      </Typography>

      <Typography component="div" variant="display"  >
        ulrich code
      </Typography>

      <Typography component="div" variant="caption2"  >
        ulrich code
      </Typography>

      <Typography component="div" variant="caption4" weight="medium" >
        ulrich code
      </Typography>

    </div>
*/} </Container></>

    )
}
