export type LinkTypes = "internal" | "external";

export const linkType: Record<string, LinkTypes> = {
    INTERNAL: "internal",
    EXTERNAL: "external"
}