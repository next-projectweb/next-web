import clsx from "clsx";
import React from "react"

interface props {

    children?: React.ReactNode
    className?: string


}

export const Container = ({ children, className }: props) => {

    return (
        <div className={clsx(className, "max-w-7xl     mx-auto space-y-5 w-full")}>
            {children}
        </div >
    );

}