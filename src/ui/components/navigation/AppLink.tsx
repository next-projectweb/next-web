import { AppLinks } from "@/types/apps-links";
import { FaSlack } from "react-icons/fa";
import { IoLogoYoutube } from "react-icons/io";
import { FaLinkedin } from "react-icons/fa";



export const footerApplicationLinks: AppLinks[] = [
    {
        label: "Accueils",
        baseUrl: "/",
        type: "internal"
    },
    {
        label: "Projets",
        baseUrl: "/#",
        type: "internal"
    },
    {
        label: "coder Monkeys",
        baseUrl: "/#",
        type: "internal"
    },

    {
        label: "Formations",
        baseUrl: "/",
        type: "external"
    }
]

export const footerUserLinks: AppLinks[] = [
    {
        label: "Mon espace",
        baseUrl: "/",
        type: "internal"
    },
    {
        label: "Projets",
        baseUrl: "/#",
        type: "internal"
    },
    {
        label: "coder Monkeys",
        baseUrl: "/#",
        type: "internal"
    },
    {
        label: "Formations",
        baseUrl: "/",
        type: "external"
    }
]

export const footerInformationLinks: AppLinks[] = [
    {
        label: "CGU",
        baseUrl: "/",
        type: "internal"
    },
    {
        label: "Projets",
        baseUrl: "/#",
        type: "internal"
    },
    {
        label: "coder Monkeys",
        baseUrl: "/#",
        type: "internal"
    },
    {
        label: "Formations",
        baseUrl: "/",
        type: "external"
    }
]

export const footerSocialLinks: AppLinks[] = [
    {
        label: "Youtube",
        baseUrl: "/",
        type: "external",
        icon: IoLogoYoutube
    },

    {
        label: "Linkdin",
        baseUrl: "/#",
        type: "internal",
        icon: FaLinkedin
    },

    {
        label: "Slack",
        baseUrl: "/#",
        type: "internal",
        icon: FaSlack
    }

]


export const footerLinks = [
    {
        label: "App",
        links: footerApplicationLinks
    },
    {
        label: "Utilisateur",
        links: footerUserLinks
    },
    {
        label: "Informations",
        links: footerInformationLinks
    }, {
        label: "Reseaux",
        links: footerSocialLinks
    },
]
