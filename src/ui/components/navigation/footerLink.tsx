import { Typography } from '../../design-system/typographie/typography';

export const footerLink = () => {

  return (
    <div className=" min-w-[190px]">
      <Typography variant="caption2" className="text-white pb-5">Titre</Typography>
      <Typography variant="caption3" className="space-y-4 text-gray-800">
        <div className="text-primary-600">Noeud 1</div>
        <div>Noeud 1</div>
        <div>Noeud 1</div>
        <div>Noeud 1</div>
      </Typography>
    </div>
  );
};
