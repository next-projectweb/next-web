import { link } from "fs";
import { footerSocialLinks } from "./AppLink";
import { Buttons } from "@/ui/design-system/button/Buttons";
import { uuid } from "uuidv4";
import { RiFacebookBoxFill, RiFacebookFill } from "react-icons/ri";
import { FaYoutube } from "react-icons/fa";
import { FaUser } from "react-icons/fa6";
import clsx from "clsx";

interface props {
    theme?: "accent" | "secondary" | "gray";
    className?: string;
}


export const SocialNetworkButton = ({ className, theme = "accent" }: props) => {

    const icoList = footerSocialLinks.map((SacialNetwork) => {

        return <Buttons

            key={uuid()}
            variant="ico"
            icontheme={theme}
            icon={{
                icon: SacialNetwork.icon ? SacialNetwork.icon : RiFacebookFill
            }}
        />

    });

    return <div className={clsx(className, " flex  items-center gap-2")}>{icoList}</div>;

}