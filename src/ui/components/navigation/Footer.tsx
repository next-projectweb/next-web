import { Typography } from '../../design-system/typographie/typography';
import Image from "next/image";
import monSVG from "/public/assets/svg/YTB.svg";
import { footerApplicationLinks, footerInformationLinks, footerLinks, footerSocialLinks, footerUserLinks } from "./AppLink";
import { AppLinks, FooterLinks } from "@/types/apps-links";
import { uuid } from "uuidv4";
import { Container } from '../container/container';
import React from 'react';
import { ActiveLink } from './activeLink';
import clsx from 'clsx';
import { linkType } from '@/lib/link-type';
import { SocialNetworkButton } from './Social-Network-buttons';


export const Footer = () => {

    // console.log(uuid())
    const currentYear = new Date().getFullYear();
    const footerNavigationList = footerLinks.map((data) => {
        return (

            <FooterLink key={uuid()} data={data} />
        );


    })

    return (

        <div className=" bg-gray ">

            <Container className="text-white flex  justify-between pt-16" >
                <div className="  flex flex-col items-center gap-1">
                    <Typography variant="caption1" >
                        Formation gratuites
                    </Typography>
                    <Typography variant="caption3" className=" text-gray-600">
                        Abonne-toi a ma chaine!
                    </Typography>

                    <a href="https://youtube.com/@remotemonkey" target="_blank">
                        <Image src={monSVG} width={229} height={216} alt="formation youtube" />
                    </a>
                </div>

                <div className=" text-white">
                    <div className="  flex gap-7" >
                        {footerNavigationList}
                    </div>
                </div>


            </Container>
            <Container className=" pt-9  pb-11 space-y-11" >
                <hr className=" text-gray-800" />
                <div className=" flex  items-center justify-between">
                    <Typography variant="caption4" className="text-gray-700">
                        {`Copyright © ${currentYear}| Propulsed by `}

                        <a href="#" className=" underline" target="_blank"> Arnaud desportes </a>{` - Remote monkey SASU`}
                    </Typography>
                    <div > <SocialNetworkButton theme='gray' /> </div>

                </div>
            </Container>

        </div>);

}

interface footer_LinkPrpos {
    data: FooterLinks;
}

const FooterLink = ({ data }: footer_LinkPrpos) => {

    const link = data.links.map((link) => {
        return (
            <div key={uuid()}>
                {link.type === linkType.INTERNAL && (<ActiveLink href={link.baseUrl} >{link.label}</ActiveLink>
                )}
                {link.type === linkType.EXTERNAL && (<a href={link.baseUrl} target="_blank" >{link.label}</a>)}

            </div>);


    })

    return (
        <div className=" min-w-[190px]">
            <Typography variant="caption2" className=" text-white pb-5" > {data.label}</Typography>

            <Typography variant="caption3" className=" space-y-4 text-gray-600">
                {link}
            </Typography>

        </div>
    );
}

