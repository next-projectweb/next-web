import { Container } from "@/ui/components/container/container";
import { Buttons } from "@/ui/design-system/button/Buttons";
import { Logo } from "@/ui/design-system/logo/Logo";
import { Typography } from "@/ui/design-system/typographie/typography";
import Link from "next/link";
import { ActiveLink } from "./activeLink";

interface props { }

export const Navigaion = ({ }: props) => {

    return (
        <div className=" border-b-2 border-gray-400">
            <Container className="py-1 flex justify-between gap-7">
                <Link href="/">
                    <div className=" flex items-center gap-2.5">
                        <Logo size="small" />
                        <div className=" flex flex-col ">
                            <div className="text-gray font-extrabold  text-[24px]">Ulrich Monkey</div>
                            <Typography variant="caption4" components="span">trouve l inspriration et recois des fillbacks</Typography>
                        </div>
                    </div>
                </Link>
                <div className=" flex items-center gap-7">
                    <Typography variant="caption3" components="div" className="flex items-center gap-7">
                        <ActiveLink href="/design-system">Design system</ActiveLink>
                        <ActiveLink href="/projets">Projets</ActiveLink>
                        <ActiveLink href="/formation">Formations</ActiveLink>
                        <ActiveLink href="/contact">contacts</ActiveLink>
                    </Typography>
                    <div className="flex items-center gap-2">
                        <Buttons size="small" >Connexion</Buttons>
                        <Buttons size="small" variant="secondary">Rejoindre</Buttons>
                    </div>

                </div>
            </Container>

        </div>
    );

}