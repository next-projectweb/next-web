import Head from "next/head";


interface  props{

    title? : string;
    description?: string;

}

export const Seo = ({title, description}: props) =>{

    return (
       
       <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
         <meta name="viewport" content={description} property="" />
      </Head>
      
    );
    
}
