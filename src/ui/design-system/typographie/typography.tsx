import clsx from "clsx";

interface Props {
    variant?: "display" | "h1" | "h2" | "h3" | "h4" | "h5" | "lead" | "body-lg" | "body-md" | "body-base" | "body-sm" | "caption1" | "caption2" | "caption3" | "caption4" | "caption5";
    components?: "h1" | "h2" | "h3" | "h4" | "h5" | "div" | "p" | "span";
    children: React.ReactNode;
    className?: string;
}

export const Typography = ({ variant = "h3", children, components, className }: Props) => {
    let variantStyle: string = "";

    switch (variant) {
        case "display":
            variantStyle = "text-8xl";
            break;
        case "h1":
            variantStyle = "text-7xl";
            break;
        case "h2":
            variantStyle = "text-6xl";
            break;
        case "h3": // default
            variantStyle = "text-5xl";
            break;
        case "h4":
            variantStyle = "text-4xl";
            break;
        case "h5":
            variantStyle = "text-3xl";
            break;
        case "lead":
            variantStyle = "text-2xl";
            break;
        case "body-lg":
            variantStyle = "text-lg";
            break;
        case "body-md":
            variantStyle = "text-md";
            break;
        case "body-base":
            variantStyle = "text-base";
            break;
        case "body-sm":
            variantStyle = "text-sm";
            break;
        case "caption1":
            variantStyle = "text-caption1";
            break;
        case "caption2":
            variantStyle = "text-caption2";
            break;
        case "caption3":
            variantStyle = "text-caption3";
            break;
        case "caption4":
            variantStyle = "text-caption4";
            break;
    }

    return (
        <div className={clsx(variantStyle, className)} >
            {children}
        </div>
    );
};