import { iconProps } from "@/types/iconProps";
import clsx from "clsx";
import { Component } from "react";
import { Spinner } from "../spinner/Spinner";


interface props {

    size?: "small" | "medium" | "large"
    variant?: "accent" | "secondary" | "outline" | "disableds" | "ico"
    icon?: iconProps;
    icontheme?: "accent" | "secondary" | "gray";
    iconPosition?: "left" | "right"
    desable?: boolean
    isloading?: boolean
    children?: React.ReactNode;

}

export const Buttons = ({

    size = "medium",
    variant = "accent",
    icon,
    icontheme = "accent",
    iconPosition = "right",
    desable = false,
    isloading,
    children

}: props) => {

    let variantStyle: string = "",
        sizeStyle: string = "",
        icoSize: number = 0;

    switch (variant) {
        case "accent":
            variantStyle = "bg-primary hover:bg-primary-400 text-white rounded  ";
            break;

        case "secondary":
            variantStyle = "bg-primary-200 hover:bg-primary-300/50 text-primary rounded";
            break;

        case "outline":
            variantStyle = "bg-white hover:bg-gray-400/50 border border-gray-500 text-gray-900 rounded";
            break;
        case "disableds":
            variantStyle = "bg-gray-400 border border-gray-500  text-gray-600 rounded cursor-not-allowed";
            break;

        case "ico":
            if (icontheme === "accent") {

                variantStyle = "bg-primary hover:bg-primary-400 text-white rounded-full"

            }

            if (icontheme === "secondary") {

                variantStyle = "bg-primary-200 hover:bg-primary-300/50 text-primary rounded-full"

            }

            if (icontheme === "gray") {

                variantStyle = "bg-gray-800 hover:bg-gray-700/50 text-white rounded-full"

            }



            break;
    }

    switch (size) {
        case "large":
            sizeStyle = `ext-caption1 font-medium ${variant === "ico" ? " flex items-center justify-center w-[60px] h-[60px]" : "px-[22px] py-[18px]"} `;
            icoSize = 24;
            break;

        case "medium": //default size is smaller 
            sizeStyle = `text-caption2  font-medium ${variant === "ico" ? " flex items-center justify-center w-[50px] h-[50px]" : "px-[18px] py-[15px]"}`;
            break;
        case "small":
            sizeStyle = `text-caption3 font-medium ${variant === "ico" ? " flex items-center justify-center w-[40px] h-[40px]" : "px-[14px] py-[11px]"
                }`;
            icoSize = 18;
            break;
    }


    return (

        <>
            <button
                type="button"
                className={clsx("animate", variantStyle, icoSize, sizeStyle, isloading && "cursor-wait relative ")}
                onClick={() => alert("bonjour")}
                disabled={desable}>


                {isloading && (
                    <div className="absolute inset-0 flex items-center justify-center" >
                        <Spinner size="small" />
                    </div>
                )}
                <div className={clsx(isloading && "invisible")}>
                    {icon && variant === "ico" ? (

                        <icon.icon size={icoSize} />

                    ) : (


                        <div className={clsx(icon && "flex items-center gap-1")} >

                            {icon && iconPosition === "left" && (
                                <icon.icon size={icoSize} />
                            )}
                            {children}
                            {icon && iconPosition === "right" && (
                                <icon.icon size={icoSize} />
                            )}

                        </div>
                    )}
                </div>
            </button>
        </>
    );


}