import clsx from "clsx";
import { BiLoader } from "react-icons/bi";
import { LuLoader2 } from "react-icons/lu";




interface Props {
    size?: "small" | "medium" | "large"
    variant?: "primary" | "white"

}

export const Spinner = ({ size = "small", variant = "primary" }: Props) => {
    let variantStyle: string, sizeStyle: string;

    switch (size) {
        case "small":
            sizeStyle = "w-5 h-5";
            break;
        case "medium":
            sizeStyle = "w-9 h-9";
            break;

        case "large":
            sizeStyle = "w-12 h-12";
            break;

    }

    switch (variant) {
        case "primary": // default
            variantStyle = "text-yellow";
            break;
        case "white":
            variantStyle = "text-green";
            break;


    }

    return (

        <LuLoader2 className={clsx(sizeStyle, variantStyle, "animate-spin")} />

    );


}

