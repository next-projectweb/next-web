import clsx from "clsx";
import Image from "next/image";

interface props {
    size?: "very-small" | "medium" | "large" | "small";
    src: string;
    alt: string;
}

export const Avartar = ({ size = "medium", src, alt }: props) => {

    let seziStyle: string = "";

    switch (size) {

        case "medium": // default 
            seziStyle = "w-[34px] h-[34px]"
            break;
        case "large":
            seziStyle = "w-[50px] h-[50px]"
            break;
        case "small":
            seziStyle = "w-[24px] h-[24px]"

            break;

    }
    return <div className={clsx(seziStyle, "bg-gray-400 rounded-full relative")} >
        <Image
            src={src}
            alt="avatar"
            fill
            className="rounded-full object-center object-cover"
        /></div>;

}
